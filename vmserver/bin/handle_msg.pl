#!/usr/bin/perl -w
# GCLC 2015
# SIGMA Informatique
#
# Team : F.GESTIN, S.ROMAIN, M.TALLET, E.SEITE
#
#

use strict;
use File::Copy;
use IO::Socket;
use Compress::Zlib;
# use Data::Dumper;

# Time between two messages
my $PORT = 46666;

# -- flush every write
$| = 1;

my $f_tmp = "/opt/gclc/gclc.log";

# -- Passage en mode demon
my $listen_msg = IO::Socket::INET->new( Proto     => 'tcp',
                                 LocalPort => $PORT,
                                 Listen    => SOMAXCONN,
                                 Reuse     => 1);
# Cant Start Daemon
die "can't setup server" unless $listen_msg;

while (1) {
        my $client_listen_msg = $listen_msg->accept();
        # -- autoflush
        $listen_msg->autoflush(1);

        # -- current msg begin
        open MSG, ">$f_tmp" || die "Error on open log file $f_tmp $!";

        my $msg = '';
        # get the job : get data (don't write on disk for line))
        while (my $msg_line = <$client_listen_msg>) {
                $msg .= $msg_line;
        }
		# uncompress data
		my $msg_uncompress = uncompress($msg);
        # do the job : write msg on disk
        print MSG $msg_uncompress;

        close MSG;
}

$listen_msg->close();