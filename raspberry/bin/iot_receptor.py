#!/usr/bin/python3 -u
import socket
import sys
import datetime
import threading
import zlib
import time
import queue

ZLIB_COMP_LEVEL = 3

HOST = ''    # Symbolic name meaning all available interfaces
PORT = 514  # Replace syslog/udp
#PORT = 5514  # Replace syslog/udp

VMHOST = '51.255.62.68' #Destiation to push to
#VMHOST = '127.0.0.1' #Destiation to push to
VMPORT = 46666          #destination port to push to

#Temps d'attente de 'pause de données'
WAITING_STUFF = 60

#Un test d'envoi toutes les 10 secondes en cas de souci
#max 2mn
SENDING_TRIES=12
TRIES_DELAY=10

LAST_RECEIVED_TIME = None

#Preparing compression object
#Preparing listen socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
msgqueue = queue.Queue()
#print('Starting iot_receptor')
 
#Bind socket to local host and port
try:
    s.bind((HOST, PORT))
except socket.error as msg:
    msgerr='Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    print(msgerr)
    sys.exit()

#Function to transform data
def translatebyte(mydata):
  # Brutal method :(
  mydatatemp = mydata.decode(encoding="utf-8", errors="ignore")
  mydatatemp = mydatatemp.rstrip('\n')
  mydatatemp = mydatatemp.replace('\n','#012')
  mydatatemp = mydatatemp.replace('\t','#011')
  mydatatemp += "\n"
  return bytes(mydatatemp, 'UTF-8')
#end Function translatebyte(mydata)

#Function for sending data
def sendServer(data):
  tryes=0
  clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  while tryes < SENDING_TRIES:
    try:
      clientsocket.connect((VMHOST, VMPORT))

      compressdata=zlib.compress(data, ZLIB_COMP_LEVEL)

      clientsocket.send(compressdata)
      break;
    except ConnectionRefusedError:
      tryes += 1
      #print("no server found, waiting " + str(TRIES_DELAY) + " seconds (" + str(tryes) + ")")
      time.sleep(TRIES_DELAY)
    except:
      #exception non geree, on s'arrete
      myexcept = sys.exc_info()
      print(myexcept)
      break;
  #fin du while
  if tryes >= SENDING_TRIES:
    print("Cancelling send, data are lost, sorry :(")
  clientsocket.close()
# end Function def sendServer(data):
 
def consummeQueue():
  mydata=bytes()
  mylastdatalen = 0
  myactualdatalen = 0
  #On s'assure de demarrer apres l'ecoute reseau
  while 1:
    mytimesref = LAST_RECEIVED_TIME
    mytimestamp = datetime.datetime.now()
    if mytimesref != None:
      mydelta = mytimestamp - mytimesref
      if mydelta.seconds < WAITING_STUFF:
         waitingtime = WAITING_STUFF - mydelta.seconds
         #print("Not the last data, waiting " + str(waitingtime))
         time.sleep(waitingtime)
         continue
    else:
      #print("Waiting first")
      time.sleep(WAITING_STUFF)
      continue
    if msgqueue.empty():
      if mydata:
        #print("sending :")
        #print(mydata) 
        sendServer(mydata)
        mydata=bytes()
      else:
        #print("Waiting new stuff")
        time.sleep(WAITING_STUFF)
        continue
    else:
      try:
        #Plus de caracteres speciaux, on economise du CPU
        #mydatatemp = msgqueue.get()
        #mydata += translatebyte(mydatatemp)
        mydata += msgqueue.get()
        msgqueue.task_done()
      except queue.Empty:
        continue
    # fin du if
  # fin du while
# end Function consummeQueue()

#Launching a thread to consumme queue
t = threading.Thread(target=consummeQueue)
t.daemon = True
t.start()

#now listening as syslog
try:
  while 1:
    data, addr = s.recvfrom(1024)

    if not data:
      break
    mydate = datetime.datetime.now()
    LAST_RECEIVED_TIME = mydate
    mydateformat = mydate.strftime('%b  %-d %H:%M:%S ')
    datatosend = bytes(mydateformat, 'UTF-8') + data
    msgqueue.put(datatosend)
except:
  myexcept = sys.exc_info()
  print(myexcept)
finally: 
  s.close()
